# React Native Mobile App

## Downloading

```
git clone https://liza-veis@bitbucket.org/liza-veis/mobile-test-task.git
```

## Running application

1. Go to the application folder

2. Run `npm i`

3. After installation run `npx react-native start`

4. Open a new terminal inside project folder and run `npx react-native run-android`

## Sign In

Default email - **user**

Default password - **querty**
