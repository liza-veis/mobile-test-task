export const searchContacts = (contacts: UserData[], search: string) =>
  contacts.filter(({name, email}) => {
    const isNameMatch = name.search(search) !== -1;
    const isEmailMatch = email.search(search) !== -1;

    return isNameMatch || isEmailMatch;
  });
