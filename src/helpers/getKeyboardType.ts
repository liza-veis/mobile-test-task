import {KeyboardTypeOptions} from 'react-native';

export const getKeyboardType = (type: string): KeyboardTypeOptions => {
  switch (type) {
    case 'email': {
      return 'email-address';
    }

    case 'tel': {
      return 'phone-pad';
    }

    default: {
      return 'default';
    }
  }
};
