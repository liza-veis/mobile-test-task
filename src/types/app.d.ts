type AuthData = {
  email: string;
  password: string;
};

type UserData = {
  id: string;
  name: string;
  email: string;
  phone: string;
};

type ContactsStackParamList = {
  ContactList: undefined;
  CreateContact: undefined;
  ContactInfo: {user: UserData; isEdited?: boolean};
};
