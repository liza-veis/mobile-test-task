import {ADMIN_EMAIL, ADMIN_PASSWORD} from '../common/config';

export class UserService {
  authorize({email, password}: AuthData) {
    const isRightEmail = email === ADMIN_EMAIL;
    const isRightPassword = password === ADMIN_PASSWORD;

    if (isRightEmail && isRightPassword) {
      return true;
    }
    return false;
  }
}
