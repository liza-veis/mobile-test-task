import uuid from 'react-native-uuid';
import {USERS_DATA_URL} from '../common/config';

export class ContactService {
  async loadContacts() {
    const res = await fetch(USERS_DATA_URL);
    const data: UserData[] = await res.json();

    return data;
  }

  createContact({name, email, phone}: Omit<UserData, 'id'>): UserData {
    const id = uuid.v4().toString();
    return {id, name, email, phone};
  }
}
