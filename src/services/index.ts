import {ContactService} from './ContactService';
import {UserService} from './UserService';

export const userService = new UserService();

export const contactService = new ContactService();
