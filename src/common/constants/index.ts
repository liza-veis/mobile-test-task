export * from './colors';

export enum SCREEN_NAMES {
  ContactList = 'ContactList',
  ContactInfo = 'ContactInfo',
  CreateContact = 'CreateContact',
}
