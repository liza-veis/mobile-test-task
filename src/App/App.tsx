import 'react-native-gesture-handler';
import React from 'react';
import {Provider} from 'react-redux';

import {store} from '../store';
import {RootNavigator} from '../navigations/RootNavigator';

export const App = () => {
  return (
    <Provider store={store}>
      <RootNavigator />
    </Provider>
  );
};
