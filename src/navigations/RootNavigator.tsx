import React, {FC} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {AuthScreen} from '../screens';
import {useSelector} from 'react-redux';
import {selectIsAuthorized} from '../screens/Auth/authSelectors';
import {ContactsNavigator} from './ContactsNavigator';

export const RootNavigator: FC = () => {
  const isAuthorized = useSelector(selectIsAuthorized);

  return (
    <NavigationContainer>
      {isAuthorized ? <ContactsNavigator /> : <AuthScreen />}
    </NavigationContainer>
  );
};
