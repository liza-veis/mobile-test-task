import React, {FC} from 'react';
import {View} from 'react-native';
import {
  createStackNavigator,
  StackNavigationOptions,
} from '@react-navigation/stack';
import {ContactListScreen, CreateContactScreen} from '../screens';
import {ContactInfoScreen} from '../screens';
import {SCREEN_NAMES} from '../common/constants';

const Stack = createStackNavigator<ContactsStackParamList>();

const commonOptions: StackNavigationOptions = {
  headerTitleStyle: {alignSelf: 'center', flex: 1},
};

export const ContactsNavigator: FC = () => (
  <Stack.Navigator>
    <Stack.Screen
      name={SCREEN_NAMES.ContactList}
      component={ContactListScreen}
      options={{title: 'Contacts', ...commonOptions}}
    />
    <Stack.Screen
      name={SCREEN_NAMES.ContactInfo}
      component={ContactInfoScreen}
      options={{
        title: 'Contact info',
        ...commonOptions,
        headerRight: () => <View />,
      }}
    />
    <Stack.Screen
      name={SCREEN_NAMES.CreateContact}
      component={CreateContactScreen}
      options={{
        title: 'Add contact',
        ...commonOptions,
        headerLeft: () => null,
      }}
    />
  </Stack.Navigator>
);
