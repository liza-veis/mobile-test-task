import {authReducer} from '../screens/Auth/authReducer';
import {contactListReducer} from '../screens/ContactList';

export const rootReducer = {
  profile: authReducer,
  contactList: contactListReducer,
};
