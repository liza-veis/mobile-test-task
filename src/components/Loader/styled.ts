import styled from 'styled-components/native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {COLOR_ACCENT, COLOR_WHITE} from '../../common/constants';

export const Loader = styled.View`
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
  background-color: ${COLOR_WHITE};
  padding-bottom: 100px;
`;

export const LoaderIcon = styled(FontAwesomeIcon)`
  color: ${COLOR_ACCENT};
`;

export const Title = styled.Text`
  margin-bottom: 15px;
  font-size: 24px;
  font-weight: bold;
  color: ${COLOR_ACCENT};
`;
