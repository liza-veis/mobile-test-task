import React, {FC, useEffect, useRef} from 'react';
import {Animated, Easing} from 'react-native';
import {faSpinner} from '@fortawesome/free-solid-svg-icons';
import * as S from './styled';

type LoaderProps = {
  isLoading: boolean;
};

export const Loader: FC<LoaderProps> = ({isLoading, children}) => {
  const rotationDegree = useRef(new Animated.Value(0)).current;

  const rotate = rotationDegree.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg'],
  });

  useEffect(() => {
    Animated.loop(
      Animated.timing(rotationDegree, {
        toValue: 1,
        duration: 1500,
        easing: Easing.linear,
        useNativeDriver: true,
      }),
    ).start();
  }, [rotationDegree]);

  return isLoading ? (
    <S.Loader>
      <S.Title>Loading</S.Title>
      <Animated.View style={{transform: [{rotate}]}}>
        <S.LoaderIcon icon={faSpinner} size={40} />
      </Animated.View>
    </S.Loader>
  ) : (
    <>{children}</>
  );
};
