import React, {FC} from 'react';
import {StyleProp, ViewStyle} from 'react-native';
import * as S from './styled';

type ButtonProps = {
  name: string;
  accent?: boolean;
  style?: StyleProp<ViewStyle>;
  onPress: () => void;
};

export const Button: FC<ButtonProps> = ({name, onPress, accent, style}) => {
  return (
    <S.Button onPress={onPress} accent={accent} style={style}>
      <S.ButtonText>{name}</S.ButtonText>
    </S.Button>
  );
};
