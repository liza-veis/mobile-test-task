import styled from 'styled-components/native';
import {COLOR_ACCENT, COLOR_GRAY, COLOR_WHITE} from '../../common/constants';

type ButtonProps = {
  accent?: boolean;
};

export const Button = styled.TouchableOpacity<ButtonProps>`
  padding: 10px;
  min-width: 120px;
  align-items: center;
  background-color: ${({accent}) => (accent ? COLOR_ACCENT : COLOR_GRAY)};
  border-radius: 5px;
`;

export const ButtonText = styled.Text`
  color: ${COLOR_WHITE};
  font-weight: bold;
`;
