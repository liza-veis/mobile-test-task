import styled from 'styled-components/native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {COLOR_ACCENT, COLOR_GRAY} from '../../common/constants';

export const ContactItem = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  padding: 15px;
  border-bottom-width: 1px;
  border-bottom-color: ${COLOR_GRAY};
`;

export const UserIcon = styled(FontAwesomeIcon)`
  margin-right: 15px;
  color: ${COLOR_GRAY};
`;

export const UserName = styled.Text`
  font-size: 16px;
  font-weight: bold;
`;

export const UserEmail = styled.Text`
  margin-right: 15px;
`;

export const ButtonsContainer = styled.View`
  flex-direction: row;
  margin-left: auto;
`;

export const Button = styled.TouchableOpacity`
  padding: 10px;
`;

type ButtonIconProps = {
  accent?: boolean;
};

export const ButtonIcon = styled(FontAwesomeIcon)<ButtonIconProps>`
  color: ${({accent}) => (accent ? COLOR_ACCENT : COLOR_GRAY)};
`;
