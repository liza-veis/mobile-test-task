import React, {FC} from 'react';
import {View} from 'react-native';
import {
  faUserCircle,
  faEdit,
  faTrashAlt,
} from '@fortawesome/free-solid-svg-icons';
import * as S from './styled';

type ContactItemProps = UserData & {
  onPress: () => void;
  onEdit: () => void;
  onDelete: () => void;
};

export const ContactItem: FC<ContactItemProps> = ({
  name,
  email,
  onPress,
  onEdit,
  onDelete,
}) => (
  <S.ContactItem onPress={onPress}>
    <S.UserIcon icon={faUserCircle} size={32} />
    <View>
      <S.UserName>{name}</S.UserName>
      <S.UserEmail>{email}</S.UserEmail>
    </View>
    <S.ButtonsContainer>
      <S.Button onPress={onEdit}>
        <S.ButtonIcon icon={faEdit} size={18} accent />
      </S.Button>
      <S.Button onPress={onDelete}>
        <S.ButtonIcon icon={faTrashAlt} size={18} />
      </S.Button>
    </S.ButtonsContainer>
  </S.ContactItem>
);
