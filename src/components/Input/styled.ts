import styled from 'styled-components/native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {COLOR_ACCENT, COLOR_BLACK, COLOR_GRAY} from '../../common/constants';

type LabelProps = {
  disabled?: boolean;
};

export const Label = styled.Text<LabelProps>`
  color: ${({disabled}) => (disabled ? COLOR_BLACK : COLOR_ACCENT)};
  font-weight: bold;
`;

export const InputWrapper = styled.View`
  justify-content: center;
`;

export const Input = styled.TextInput`
  color: ${COLOR_BLACK};
  padding: 5px 0;
  border-bottom-color: ${COLOR_GRAY};
  border-bottom-width: 1px;
`;

export const VisibilityIcon = styled(FontAwesomeIcon)`
  color: ${COLOR_GRAY};
  position: absolute;
  right: 0px;
`;
