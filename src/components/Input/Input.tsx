import React, {FC, useState} from 'react';
import {
  StyleProp,
  TouchableWithoutFeedback,
  View,
  ViewStyle,
} from 'react-native';
import {faEye, faEyeSlash} from '@fortawesome/free-solid-svg-icons';
import {getKeyboardType} from '../../helpers/getKeyboardType';
import * as S from './styled';

type InputTypes = 'email' | 'password' | 'off' | 'tel';

type InputProps = {
  type: InputTypes;
  value: string;
  label: string;
  secure?: boolean;
  disabled?: boolean;
  style?: StyleProp<ViewStyle>;
  onChangeText: (text: string) => void;
};

export const Input: FC<InputProps> = ({
  type,
  value,
  label,
  style,
  secure,
  disabled,
  onChangeText,
}) => {
  const [isTextHidden, setIsTextHidden] = useState(true);

  const keyboardType = getKeyboardType(type);

  const changeTextVisibility = () => setIsTextHidden(!isTextHidden);

  return (
    <View style={style}>
      <S.Label disabled={disabled}>{label}</S.Label>
      <S.InputWrapper>
        <S.Input
          value={value}
          onChangeText={onChangeText}
          secureTextEntry={secure && isTextHidden}
          keyboardType={keyboardType}
          autoCompleteType={type}
          editable={!disabled}
          autoCorrect={false}
        />
        {secure ? (
          <TouchableWithoutFeedback onPress={changeTextVisibility}>
            <S.VisibilityIcon
              size={20}
              icon={isTextHidden ? faEye : faEyeSlash}
            />
          </TouchableWithoutFeedback>
        ) : (
          <></>
        )}
      </S.InputWrapper>
    </View>
  );
};
