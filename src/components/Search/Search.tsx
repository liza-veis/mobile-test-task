import React, {FC} from 'react';
import {StyleProp, ViewStyle} from 'react-native';
import {faSearch} from '@fortawesome/free-solid-svg-icons';
import * as S from './styled';

type SearchProps = {
  value: string;
  style?: StyleProp<ViewStyle>;
  onChangeText: (text: string) => void;
};

export const Search: FC<SearchProps> = ({value, style, onChangeText}) => {
  return (
    <S.SearchWrapper style={style}>
      <S.SearchIcon size={18} icon={faSearch} />
      <S.SearchInput
        value={value}
        onChangeText={onChangeText}
        autoCorrect={false}
      />
    </S.SearchWrapper>
  );
};
