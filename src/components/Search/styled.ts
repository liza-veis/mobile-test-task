import styled from 'styled-components/native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {COLOR_ACCENT, COLOR_GRAY} from '../../common/constants';

export const SearchWrapper = styled.View`
  justify-content: center;
`;

export const SearchInput = styled.TextInput`
  padding: 8px 8px 8px 45px;
  border: 1px solid ${COLOR_GRAY};
  border-radius: 30px;
`;

export const SearchIcon = styled(FontAwesomeIcon)`
  position: absolute;
  color: ${COLOR_ACCENT};
  left: 15px;
`;
