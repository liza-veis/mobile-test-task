import styled from 'styled-components/native';

export const Form = styled.View`
  margin: 0 auto;
  max-width: 280px;
  width: 100%;
`;
