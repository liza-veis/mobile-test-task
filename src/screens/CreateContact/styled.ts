import styled from 'styled-components/native';
import {Input} from '../../components/Input';

export const Screen = styled.View`
  align-items: center;
  padding: 50px 20px 20px;
`;

export const AddContactInput = styled(Input)`
  margin-bottom: 20px;
`;

export const ButtonsContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-top: 10px;
`;
