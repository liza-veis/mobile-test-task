import {StackNavigationProp} from '@react-navigation/stack';
import React, {FC, useState} from 'react';
import {useDispatch} from 'react-redux';
import {SCREEN_NAMES} from '../../common/constants';
import {Button, Form} from '../../components';
import {contactService} from '../../services';
import {addContact} from '../ContactList/contactListActions';
import * as S from './styled';

type CreateContactScreenProps = {
  navigation: StackNavigationProp<ContactsStackParamList>;
};

export const CreateContactScreen: FC<CreateContactScreenProps> = ({
  navigation,
}) => {
  const dispatch = useDispatch();

  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');

  const handleContactAdd = () => {
    if (!name.trim() || !phone.trim()) {
      return;
    }
    const contact = contactService.createContact({name, phone, email});
    dispatch(addContact(contact));
    navigation.navigate(SCREEN_NAMES.ContactList);
  };

  const handleCancel = () => {
    navigation.navigate(SCREEN_NAMES.ContactList);
    setName('');
    setPhone('');
    setEmail('');
  };

  return (
    <S.Screen>
      <Form>
        <S.AddContactInput
          type="off"
          label="Name"
          value={name}
          onChangeText={setName}
        />
        <S.AddContactInput
          type="tel"
          label="Phone number"
          value={phone}
          onChangeText={setPhone}
        />
        <S.AddContactInput
          type="email"
          label="Email"
          value={email}
          onChangeText={setEmail}
        />
        <S.ButtonsContainer>
          <Button name="Done" onPress={handleContactAdd} accent />
          <Button name="Cancel" onPress={handleCancel} />
        </S.ButtonsContainer>
      </Form>
    </S.Screen>
  );
};
