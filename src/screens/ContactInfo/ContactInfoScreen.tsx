import {RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';
import React, {FC, useState} from 'react';
import {faUserCircle} from '@fortawesome/free-solid-svg-icons';
import {useDispatch} from 'react-redux';
import {removeContact, setContact} from '../ContactList/contactListActions';
import {SCREEN_NAMES} from '../../common/constants';
import {Button, Form} from '../../components';
import * as S from './stylled';

type ContactInfoScreenProps = {
  route: RouteProp<{params: {user: UserData; isEdited?: boolean}}, 'params'>;
  navigation: StackNavigationProp<ContactsStackParamList>;
};

export const ContactInfoScreen: FC<ContactInfoScreenProps> = ({
  route,
  navigation,
}) => {
  const {user, isEdited: initialIsEdited = false} = route.params;
  const dispatch = useDispatch();

  const [name, setName] = useState(user.name);
  const [email, setEmail] = useState(user.email);
  const [phone, setPhone] = useState(user.phone);
  const [isEdited, setIsEdited] = useState(initialIsEdited);

  const handleEdit = () => {
    if (!name.trim() || !phone.trim()) {
      return;
    }
    setIsEdited(!isEdited);
    dispatch(setContact({name, phone, email, id: user.id}));
  };

  const handleDelete = () => {
    dispatch(removeContact(user.id));
    navigation.navigate(SCREEN_NAMES.ContactList);
  };

  return (
    <S.Screen>
      <S.UserIcon size={130} icon={faUserCircle} />
      <Form>
        <S.ContactInfoInput
          type="off"
          value={name}
          label="Name"
          onChangeText={setName}
          disabled={!isEdited}
        />
        <S.ContactInfoInput
          type="tel"
          value={phone}
          label="Phone number"
          onChangeText={setPhone}
          disabled={!isEdited}
        />
        <S.ContactInfoInput
          type="email"
          value={email}
          label="Email"
          onChangeText={setEmail}
          disabled={!isEdited}
        />
        <S.ButtonsContainer>
          <Button
            name={isEdited ? 'Save' : 'Edit'}
            onPress={handleEdit}
            accent
          />
          <Button name="Delete" onPress={handleDelete} />
        </S.ButtonsContainer>
      </Form>
    </S.Screen>
  );
};
