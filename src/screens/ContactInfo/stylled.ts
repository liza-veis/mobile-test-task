import styled from 'styled-components/native';
import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import {COLOR_GRAY} from '../../common/constants';
import {Input} from '../../components/Input';

export const Screen = styled.View`
  align-items: center;
  padding: 50px 20px 20px;
`;

export const UserIcon = styled(FontAwesomeIcon)`
  margin-bottom: 40px;
  color: ${COLOR_GRAY};
`;

export const ContactInfoInput = styled(Input)`
  margin-bottom: 20px;
`;

export const ButtonsContainer = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-top: 10px;
`;
