import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import styled from 'styled-components/native';
import {COLOR_ACCENT, COLOR_GRAY, COLOR_WHITE} from '../../common/constants';

export const Screen = styled.View`
  flex: 1;
`;

export const ContactListTop = styled.View`
  padding: 20px;
  border-bottom-width: 1px;
  border-bottom-color: ${COLOR_GRAY};
`;

export const Text = styled.Text`
  margin-top: 20px;
  padding: 20px;
  font-size: 30px;
  font-weight: bold;
  color: ${COLOR_GRAY};
  text-align: center;
`;

export const AddContactButton = styled.TouchableOpacity`
  position: absolute;
  bottom: 20px;
  right: 20px;
  padding: 18px;
  border-radius: 30px;
  background-color: ${COLOR_ACCENT};
`;

export const AddContactIcon = styled(FontAwesomeIcon)`
  color: ${COLOR_WHITE};
`;
