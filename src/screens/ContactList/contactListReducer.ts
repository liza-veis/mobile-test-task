import {createReducer} from '@reduxjs/toolkit';
import {
  addContact,
  loadContacts,
  removeContact,
  setContact,
} from './contactListActions';

type State = {
  contacts: UserData[];
  isLoading: boolean;
};

const initialState: State = {
  contacts: [],
  isLoading: false,
};

export const contactListReducer = createReducer(initialState, builder => {
  builder.addCase(loadContacts.pending, state => {
    state.isLoading = true;
  });
  builder.addCase(loadContacts.fulfilled, (state, action) => {
    state.contacts = action.payload;
    state.isLoading = false;
  });
  builder.addCase(addContact, (state, action) => {
    state.contacts = state.contacts.concat(action.payload);
  });
  builder.addCase(setContact, (state, action) => {
    state.contacts = state.contacts.map(contact => {
      const {payload} = action;
      if (contact.id === payload.id) {
        return {...contact, ...payload};
      }

      return contact;
    });
  });
  builder.addCase(removeContact, (state, action) => {
    state.contacts = state.contacts.filter(({id}) => id !== action.payload);
  });
});
