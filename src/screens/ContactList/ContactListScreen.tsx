import {StackNavigationProp} from '@react-navigation/stack';
import React, {FC, useEffect, useCallback, useState} from 'react';
import {FlatList} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import {SCREEN_NAMES} from '../../common/constants';
import {ContactItem, Search, Loader} from '../../components';
import {searchContacts} from '../../helpers/searchContacts';
import {loadContacts, removeContact} from './contactListActions';
import {selectContacts, selectIsLoading} from './contactListSelectors';
import * as S from './styled';

type ContactListScreenProps = {
  navigation: StackNavigationProp<ContactsStackParamList>;
};

export const ContactListScreen: FC<ContactListScreenProps> = ({navigation}) => {
  const dispatch = useDispatch();
  const contacts = useSelector(selectContacts);
  const isLoading = useSelector(selectIsLoading);

  const [search, setSearch] = useState('');
  const [filteredContacts, setFilteredContacts] = useState<UserData[]>([]);

  const handleSearch = (text: string) => {
    setSearch(text);
    setFilteredContacts(searchContacts(contacts, text));
  };

  const handleAddButtonPress = () => {
    navigation.navigate(SCREEN_NAMES.CreateContact);
  };

  const onContactPress = useCallback(
    (user: UserData) => {
      navigation.navigate(SCREEN_NAMES.ContactInfo, {user});
    },
    [navigation],
  );

  const onContactEdit = useCallback(
    (user: UserData) => {
      navigation.navigate(SCREEN_NAMES.ContactInfo, {user, isEdited: true});
    },
    [navigation],
  );

  const onContactDelete = useCallback(
    ({id}: UserData) => {
      dispatch(removeContact(id));
    },
    [dispatch],
  );

  useEffect(() => {
    dispatch(loadContacts());
  }, [dispatch]);

  useEffect(() => {
    setSearch('');
    setFilteredContacts(contacts);
  }, [contacts]);

  return (
    <S.Screen>
      <Loader isLoading={isLoading}>
        <S.ContactListTop>
          <Search value={search} onChangeText={handleSearch} />
        </S.ContactListTop>

        {filteredContacts.length ? (
          <FlatList
            data={filteredContacts}
            renderItem={({item}) => (
              <ContactItem
                {...item}
                onPress={() => onContactPress(item)}
                onEdit={() => onContactEdit(item)}
                onDelete={() => onContactDelete(item)}
              />
            )}
            keyExtractor={({id}) => id}
          />
        ) : (
          <S.Text>Nothing found</S.Text>
        )}
        <S.AddContactButton onPress={handleAddButtonPress}>
          <S.AddContactIcon icon={faPlus} />
        </S.AddContactButton>
      </Loader>
    </S.Screen>
  );
};
