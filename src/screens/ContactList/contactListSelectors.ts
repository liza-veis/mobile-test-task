import {RootState} from '../../store';

export const selectContacts = (state: RootState) => state.contactList.contacts;
export const selectIsLoading = (state: RootState) =>
  state.contactList.isLoading;
