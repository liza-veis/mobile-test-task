import {createAction, createAsyncThunk} from '@reduxjs/toolkit';
import {contactService} from '../../services';
import {
  ADD_CONTACT,
  LOAD_CONTACTS,
  REMOVE_CONTACT,
  SET_CONTACT,
} from './contactListActionsTypes';

export const loadContacts = createAsyncThunk(LOAD_CONTACTS, () =>
  contactService.loadContacts(),
);

export const addContact = createAction(ADD_CONTACT, (contact: UserData) => ({
  payload: contact,
}));

export const setContact = createAction(
  SET_CONTACT,
  (data: Partial<UserData> & {id: string}) => ({
    payload: data,
  }),
);

export const removeContact = createAction(REMOVE_CONTACT, (id: string) => ({
  payload: id,
}));
