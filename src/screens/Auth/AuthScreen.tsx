import React, {FC} from 'react';
import {useState} from 'react';
import {useDispatch} from 'react-redux';
import {faPaw} from '@fortawesome/free-solid-svg-icons';
import {Form} from '../../components';
import {userService} from '../../services';
import {authorizeUser} from './authActions';
import * as S from './styled';

export const AuthScreen: FC = () => {
  const dispatch = useDispatch();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleAuth = () => {
    const isAuthorized = userService.authorize({email, password});
    if (isAuthorized) {
      dispatch(authorizeUser());
    }
  };

  return (
    <S.Screen>
      <Form>
        <S.MainIcon icon={faPaw} size={90} />
        <S.Title>Sign in</S.Title>
        <S.Subitle>Hi there! Nice to see you again.</S.Subitle>
        <S.AuthInput
          type="email"
          value={email}
          label="Email"
          onChangeText={setEmail}
        />
        <S.AuthInput
          type="password"
          value={password}
          label="Password"
          onChangeText={setPassword}
          secure
        />
        <S.SubmitButton name="Sign in" onPress={handleAuth} accent />
      </Form>
    </S.Screen>
  );
};
