import {createAction} from '@reduxjs/toolkit';
import {AUTHORIZE_USER} from './authActionTypes';

export const authorizeUser = createAction(AUTHORIZE_USER);
