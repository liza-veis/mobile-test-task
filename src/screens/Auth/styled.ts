import {FontAwesomeIcon} from '@fortawesome/react-native-fontawesome';
import styled from 'styled-components/native';
import {COLOR_GRAY, COLOR_WHITE} from '../../common/constants';
import {Button} from '../../components';
import {Input} from '../../components';

export const Screen = styled.View`
  padding: 15px;
`;

export const MainIcon = styled(FontAwesomeIcon)`
  align-self: center;
  margin: 40px 0;
  color: ${COLOR_GRAY};
`;

export const Title = styled.Text`
  margin-bottom: 10px;
  font-size: 24px;
  font-weight: bold;
`;

export const Subitle = styled.Text`
  margin-bottom: 30px;
`;

export const AuthInput = styled(Input)`
  margin-bottom: 15px;
`;

export const SubmitButton = styled(Button)`
  margin-top: 20px;
`;

export const ButtonText = styled.Text`
  color: ${COLOR_WHITE};
  font-weight: bold;
`;
