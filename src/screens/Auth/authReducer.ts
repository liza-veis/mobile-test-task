import {createReducer} from '@reduxjs/toolkit';
import {authorizeUser} from './authActions';

type State = {
  isAuthorized: boolean;
};

const initialState: State = {
  isAuthorized: false,
};

export const authReducer = createReducer(initialState, builder => {
  builder.addCase(authorizeUser, state => {
    state.isAuthorized = true;
  });
});
